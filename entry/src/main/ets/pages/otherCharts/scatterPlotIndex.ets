/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import title from '../title/index';

import { ScaterChartMode } from '@ohos/mpchart';
import { ScatterChart } from '@ohos/mpchart';
import { ScatterShape } from '@ohos/mpchart';
import { ScatterData } from '@ohos/mpchart';
import { ScatterDataSet } from '@ohos/mpchart';
import type { IScatterDataSet } from '@ohos/mpchart';
import { ColorTemplate } from '@ohos/mpchart';
import { XAxis, XAxisPosition } from '@ohos/mpchart';
import { YAxis, AxisDependency, YAxisLabelPosition } from '@ohos/mpchart';
import { EntryOhos } from '@ohos/mpchart';
import { JArrayList } from '@ohos/mpchart';
import { LimitLine, LimitLabelPosition } from '@ohos/mpchart';
import { CustomScatterShapeRenderer } from './CustomScatterShapeRenderer';
import { TextPaint, Utils, MyRect, MPPointF } from '@ohos/mpchart';

@Entry
@Component
@Preview
struct scatterPlotIndex {
  //标题栏菜单文本
  private menuItemArr: Array<string> = [/*'View on GitHub', 'Toggle Values', 'Toggle Icons',*/
    'Toggle Highlight', /*'Toggle PinchZoom','Toggle Auto Scale',*/'Animate X', 'Animate Y', 'Animate XY'/*,'Save to Gallery'*/];
  //标题栏标题
  private title: string = 'ScatterChartActivity'
  @State @Watch("menuCallback") model: title.Model = new title.Model()

  //标题栏菜单回调
  menuCallback() {
    if (this.model == null || this.model == undefined) {
      return
    }
    let index: number = this.model.getIndex()
    if (index == undefined || index == -1) {
      return
    }
    switch (this.menuItemArr[index]) {
      case 'View on GitHub':
      //TODO View on GitHub
        break;
      case 'Toggle Values':
        this.setShowValues()
        break;
      case 'Toggle Icons':
      //this.setShowIcons();
        break;
      case 'Toggle Highlight':
        this.setShowHighlight()
        break;
      case 'Toggle PinchZoom':
        break;
      case 'Toggle Auto Scale':
        break;
      case 'Animate X':
        this.scaterChartMode.animate('X', 1000, 20);
        break;
      case 'Animate Y':
        this.scaterChartMode.animate('Y', 1000, 20)
        break;
      case 'Animate XY':
        this.scaterChartMode.animate('XY', 1000, 20)
        break;

    }
    this.model.setIndex(-1)
  }

  topAxis: XAxis = new XAxis(); //顶部X轴
  bottomAxis: XAxis = new XAxis(); //底部X轴
  mWidth: number = 350; //表的宽度
  mHeight: number = 300; //表的高度
  minOffset: number = 15; //X轴线偏移量
  leftAxis: YAxis = null;
  rightAxis: YAxis = null;
  scatterData: ScatterData = null;
  XLimtLine: LimitLine = new LimitLine(35, "Index 10");
  @State
  scaterChartMode: ScaterChartMode = new ScaterChartMode();
  isDrawValuesEnable: boolean = false;

  public changeData() {
    this.scatterData = this.initCurveData(250, 250);
    this.scaterChartMode.data = this.scatterData;
    this.scaterChartMode.invalidate();
  }

  build() {
    Column() {
      title({ model: this.model })
      Stack({ alignContent: Alignment.TopStart }) {
        ScatterChart({
          scaterChartMode: this.scaterChartMode
        })
      }

      Button("切换数据").onClick(() => {
        this.changeData();
      }).margin(30)
    }
  }

  aboutToAppear() {
    this.model.menuItemArr = this.menuItemArr
    this.model.title = this.title
    this.scatterData = this.initCurveData(250, 250);

    this.topAxis.setLabelCount(6, false);
    this.topAxis.setPosition(XAxisPosition.TOP);
    this.topAxis.setAxisMinimum(0);
    this.topAxis.setAxisMaximum(250);
    this.topAxis.setDrawGridLines(false);

    this.bottomAxis.setLabelCount(6, false);
    this.bottomAxis.setPosition(XAxisPosition.BOTTOM);
    this.bottomAxis.setAxisMinimum(0);
    this.bottomAxis.setAxisMaximum(250);

    this.leftAxis = new YAxis(AxisDependency.LEFT);
    this.leftAxis.setLabelCount(11, false);
    this.leftAxis.setPosition(YAxisLabelPosition.OUTSIDE_CHART);
    this.leftAxis.setSpaceTop(15);
    this.leftAxis.setAxisMinimum(0);
    this.leftAxis.setAxisMaximum(250);
    this.leftAxis.enableGridDashedLine(10, 10, 0)

    this.rightAxis = new YAxis(AxisDependency.RIGHT);
    this.rightAxis.setDrawGridLines(false);
    this.rightAxis.setDrawLabels(false);
    this.rightAxis.setLabelCount(7, false);
    this.rightAxis.setSpaceTop(11);
    this.rightAxis.setAxisMinimum(0); // this replaces setStartAtZero(true)
    this.rightAxis.setAxisMaximum(250);
    this.rightAxis.setEnabled(false);

    this.scaterChartMode.setYExtraOffset(this.model.height)
    this.scaterChartMode.topAxis = this.topAxis
    this.scaterChartMode.bottomAxis = this.bottomAxis
    this.scaterChartMode.mWidth = this.mWidth
    this.scaterChartMode.mHeight = this.mHeight
    this.scaterChartMode.minOffset = this.minOffset
    this.scaterChartMode.leftAxis = this.leftAxis
    this.scaterChartMode.rightAxis = this.rightAxis
    this.scaterChartMode.data = this.scatterData
    this.scaterChartMode.XLimtLine = this.XLimtLine
    this.setDisplayRect();
    this.scaterChartMode.init()
  }

  public setDisplayRect() {
    let textPaint: TextPaint = new TextPaint();
    textPaint.setTextSize(this.scaterChartMode.leftAxis.getTextSize());
    let leftTextWidth = Utils.calcTextWidth(textPaint, this.scaterChartMode.leftAxis.getAxisMaximum() + "");
    let left = this.scaterChartMode.leftAxis.getSpaceTop() + leftTextWidth;
    let top = this.scaterChartMode.minOffset;
    let right = this.scaterChartMode.mWidth - this.scaterChartMode.minOffset - leftTextWidth;
    let bottom = this.scaterChartMode.mHeight - this.scaterChartMode.minOffset;
    this.scaterChartMode.data.mDisplayRect = new MyRect(left, top, right, bottom);
  }

  /**
   * 初始化数据
   * @param xRange  x轴范围
   * @param yRange  y轴范围
   */
  private initCurveData(xRange: number, yRange: number): ScatterData {

    let values = this.generateRandomData(xRange, yRange);
    let values2 = this.generateRandomData(xRange, yRange);
    let values3 = this.generateRandomData(xRange, yRange);

    // create a dataset and give it a type
    let set1: ScatterDataSet = new ScatterDataSet(values, "DS 1");
    set1.setScatterShape(ScatterShape.SQUARE);
    set1.setIconsOffset(new MPPointF(0, px2vp(0)));
    //    set1.setScatterShapeHoleRadius(3);
    //    set1.setScatterShape(ScatterShape.TRIANGLE);
    //    set1.setScatterShape(ScatterShape.CROSS);
    //    set1.setScatterShape(ScatterShape.CHEVRON_DOWN);
    set1.setColorByColor(ColorTemplate.COLORFUL_COLORS[0]);
    set1.setDrawValues(this.isDrawValuesEnable);
    set1.setHighlightLineWidth(px2vp(1))

    let set2: ScatterDataSet = new ScatterDataSet(values2, "DS 2");
    set2.setScatterShape(ScatterShape.CIRCLE);
    set2.setScatterShapeHoleColor(ColorTemplate.COLORFUL_COLORS[3]);
    //    set2.setScatterShapeHoleRadius(3);
    set2.setIconsOffset(new MPPointF(0, px2vp(0)));
    set2.setColorByColor(ColorTemplate.COLORFUL_COLORS[1]);
    set2.setDrawValues(this.isDrawValuesEnable);
    set2.setHighlightLineWidth(px2vp(1))

    let set3: ScatterDataSet = new ScatterDataSet(values3, "DS 3");
    //    set3.setScatterShape(ScatterShape.X);
    //    set3.setScatterShape(ScatterShape.CHEVRON_UP);
    //    set3.setScatterShape(ScatterShape.CHEVRON_DOWN);
    //    set3.setScatterShape(ScatterShape.TRIANGLE);
    //    set3.setScatterShapeHoleRadius(3);
    set3.setShapeRenderer(new CustomScatterShapeRenderer());
    //    set3.setScatterShapeHoleRadius(3);
    set3.setColorByColor(ColorTemplate.COLORFUL_COLORS[2]);
    set3.setIconsOffset(new MPPointF(0, px2vp(0)));
    set3.setDrawValues(this.isDrawValuesEnable);
    set3.setHighlightLineWidth(px2vp(1))

    set1.setScatterShapeSize(8);
    set2.setScatterShapeSize(8);
    set3.setScatterShapeSize(8);

    let dataSets: JArrayList<IScatterDataSet> = new JArrayList<IScatterDataSet>();
    dataSets.add(set1); // add the data sets
    dataSets.add(set2);
    dataSets.add(set3);

    let dataResult: ScatterData = new ScatterData(dataSets);
    dataResult.setDrawValues(this.isDrawValuesEnable);
    dataResult.setValueTextSize(8);
    dataResult.setHighlightEnabled(true);

    return dataResult;
  }

  private generateRandomData(xRange: number, yRange: number): JArrayList<EntryOhos> {
    let values = new JArrayList<EntryOhos>();
    for (let i = 0; i < 5; i++) {
      let x = Math.random() * xRange; // Random x value within specified count.
      let y = Math.random() * yRange; // Random y value within specified range.
      values.add(new EntryOhos(x, y));
    }
    return values;
  }

  public setShowValues() {
    this.isDrawValuesEnable = (!this.isDrawValuesEnable)
    this.scaterChartMode.data.setDrawValues(this.isDrawValuesEnable);
    this.scaterChartMode.drawChart();
  }

  public setShowHighlight() {
    this.scaterChartMode.data.setHighlightEnabled(!this.scaterChartMode.data.isHighlightEnabled());
    this.scaterChartMode.drawHighLight();
  }
}









