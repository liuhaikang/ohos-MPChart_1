/*
 * Copyright (C) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { XAxis, XAxisPosition } from '@ohos/mpchart'
import { YAxis, AxisDependency, YAxisLabelPosition } from '@ohos/mpchart'
import { WaterfallEntry } from '@ohos/mpchart';
import { JArrayList } from '@ohos/mpchart';
import { WaterfallDataSet } from '@ohos/mpchart';
import { WaterfallData } from '@ohos/mpchart';
import { WaterfallChart, WaterfallChartModel } from '@ohos/mpchart'
import type { IWaterfallDataSet } from '@ohos/mpchart'
import title from '../title/index';


@Entry
@Component
struct Index_waterfall_charts_2 {
  @State model: WaterfallChartModel = new WaterfallChartModel();
  mWidth: number = 350; //表的宽度
  mHeight: number = 500; //表的高度
  minOffset: number = 15; //X轴线偏移量
  leftAxis: YAxis = null;
  rightAxis: YAxis = null;
  bottomAxis: XAxis = new XAxis();
  //标题栏菜单文本
  private menuItemArr: Array<string> = ['Toggle Bar Borders', 'Animate X', 'Animate Y', 'Animate XY'];
  //标题栏标题
  private title: string = 'WaterfallChart'
  @State @Watch("menuCallback") titleModel: title.Model = new title.Model()

  //标题栏菜单回调
  menuCallback() {
    if (this.titleModel == null || this.titleModel == undefined) {
      return
    }
    let index: number = this.titleModel.getIndex()
    if (index == undefined || index == -1) {
      return
    }
    switch (this.menuItemArr[index]) {
      case 'Toggle Bar Borders':
        for (let i = 0;i < this.model.getWaterfallData().getDataSets().length(); i++) {
          let barDataSet = this.model.getWaterfallData().getDataSets().get(i) as WaterfallDataSet;
          barDataSet.setBarBorderWidth(barDataSet.getBarBorderWidth() == 1 ? 0 : 1)
        }
        this.model.invalidate()
        break;
      case 'Animate X':
        this.model.animateX(2000)
        break;
      case 'Animate Y':
        this.model.animateY(2000)
        break;
      case 'Animate XY':
        this.model.animateXY(2000, 2000)
        break;

    }
    this.titleModel.setIndex(-1)
  }

  public aboutToAppear() {
    this.titleModel.menuItemArr = this.menuItemArr
    this.titleModel.title = this.title
    this.leftAxis = new YAxis(AxisDependency.LEFT);
    this.leftAxis.setLabelCount(7, false);
    this.leftAxis.setPosition(YAxisLabelPosition.OUTSIDE_CHART);
    this.leftAxis.setSpaceTop(15);
    this.leftAxis.setAxisMinimum(40);
    this.leftAxis.setAxisMaximum(200);
    this.leftAxis.enableGridDashedLine(10, 10, 0)

    this.rightAxis = new YAxis(AxisDependency.RIGHT);
    this.rightAxis.setDrawGridLines(false);
    this.rightAxis.setLabelCount(7, false);
    this.rightAxis.setSpaceTop(11);
    this.rightAxis.setAxisMinimum(40); // this replaces setStartAtZero(true)
    this.rightAxis.setAxisMaximum(200);

    this.bottomAxis.setLabelCount(7, false);
    this.bottomAxis.setPosition(XAxisPosition.BOTTOM);
    this.bottomAxis.setAxisMinimum(0);
    this.bottomAxis.setAxisMaximum(7);

    this.setData(this.bottomAxis.getAxisMaximum(), this.leftAxis.getAxisMaximum())

    this.model.setWidth(this.mWidth);
    this.model.setHeight(this.mHeight);
    this.model.init();
    this.model.setDrawBarShadow(false);
    this.model.setDrawValueAboveBar(true);
    this.model.getDescription().setEnabled(false);
    this.model.setMaxVisibleValueCount(60);
    this.model.setLeftYAxis(this.leftAxis);
    this.model.setRightYAxis(this.rightAxis);
    this.model.setXAxis(this.bottomAxis)
    this.model.mRenderer.initBuffers();
    this.model.prepareMatrixValuePx();
    this.model.setCustomCylinderWidth(20);
    this.model.setRadius(1000);
    this.model.setCylinderCenter(true)
  }

  build() {
    Column() {
      Stack({ alignContent: Alignment.TopStart }) {
        WaterfallChart({ model: this.model })
      }
    }.alignItems(HorizontalAlign.Start)
  }

  private setData(count: number, range: number) {


    let values: JArrayList<WaterfallEntry> = new JArrayList<WaterfallEntry>();
    let minimum = this.leftAxis.getAxisMinimum()
    values.add(new WaterfallEntry(1));
    values.add(new WaterfallEntry(2, [80 - minimum, 120 - minimum], [120 - minimum]));
    values.add(new WaterfallEntry(3, [60 - minimum, 160 - minimum], [60 - minimum, 90 - minimum]));
    values.add(new WaterfallEntry(4, [90 - minimum, 150 - minimum], [95 - minimum]));
    values.add(new WaterfallEntry(5, [40 - minimum, 180 - minimum], [41 - minimum, 45 - minimum]));
    values.add(new WaterfallEntry(6, [100 - minimum, 200 - minimum], [100 - minimum, 130 - minimum]));
    values.add(new WaterfallEntry(7, [55 - minimum, 190 - minimum], [190 - minimum]));

    let set1: WaterfallDataSet;

    if (this.model.getWaterfallData() != null &&
      this.model.getWaterfallData().getDataSetCount() > 0) {
      set1 = this.model.getWaterfallData().getDataSetByIndex(0) as WaterfallDataSet;
      set1.setValues(values);
      this.model.getWaterfallData().notifyDataChanged();
      this.model.notifyDataSetChanged();

    } else {
      set1 = new WaterfallDataSet(values, "Statistics Vienna 2014");

      set1.setDrawIcons(false);
      set1.setColorsByVariable(this.getColors());
      set1.setDotsColor(0xe74c3c)
      set1.setStackLabels(["Births", "Divorces", "Marriages"]);
      set1.setValueTextSize(8)

      let dataSets: JArrayList<IWaterfallDataSet> = new JArrayList<IWaterfallDataSet>();
      dataSets.add(set1);

      let data: WaterfallData = new WaterfallData(dataSets);

      this.model.setData(data);
    }
  }

  private getColors(): number[] {

    let colors: number[] = [];

    colors.push(0x2ecc71)
    colors.push(0xf1c40f)

    return colors;
  }
}