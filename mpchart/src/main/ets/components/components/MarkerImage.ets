/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import Chart from '../charts/Chart';
import EntryOhos from '../data/EntryOhos';
import Highlight from '../highlight/Highlight';
import FSize from '../utils/FSize';
import MPPointF from '../utils/MPPointF';

//import java.lang.ref.WeakReference;

import IMarker from './IMarker';

/**
 * View that can be displayed when selecting values in the chart. Extend this class to provide custom layouts for your
 * markers.
 *
 */
export default class MarkerImage implements IMarker {

    private mContext : Object/*Context*/;
    private mDrawable : Object/*Drawable*/;

    private  mOffset : MPPointF = new MPPointF();
    private mOffset2 : MPPointF = new MPPointF();
    private mWeakChart : Object<Chart>/*WeakReference<Chart>*/;

    private mSize : FSize = new FSize();
    private mDrawableBoundsCache : Object = new Object()/*Rect = new Rect()*/;

    /**
     * Constructor. Sets up the MarkerView with a custom layout resource.
     *
     * @param context
     * @param drawableResourceId the drawable resource to render
     */
    constructor(context : Object/*Context*/, drawableResourceId : number) {
        this.mContext = context;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
        {
            this.mDrawable = this.mContext.getResources().getDrawable(drawableResourceId, null);
        }
        else
        {
            this.mDrawable = this.mContext.getResources().getDrawable(drawableResourceId);
        }
    }

    public setOffset(offset ?: MPPointF, offsetX ?: number, offsetY ?: number) : void {
        if (!offset && offsetX && offsetY) {
            this.mOffset = offset;
            if (this.mOffset == null) {
                this.mOffset = new MPPointF();
            }
        } else if (offset && !offsetX && !offsetY) {
            this.mOffset.x = offsetX;
            this.mOffset.y = offsetY;
        }
    }

//    @Override
    public getOffset() : MPPointF {
        return this.mOffset;
    }

    public setSize(size : FSize) : void {
        this.mSize = size;

        if (this.mSize == null) {
            this.mSize = new FSize();
        }
    }

    public getSize() : FSize {
        return this.mSize;
    }

    public setChartView(chart : Chart) : void {
        this.mWeakChart = new Object(chart)/*WeakReference<>(chart)*/;
    }

    public getChartView() : Chart {
        return this.mWeakChart == null ? null : this.mWeakChart.get();
    }

//    @Override
    public getOffsetForDrawingAtPoint(posX : number, posY : number) : MPPointF{

        var offset : MPPointF = this.getOffset();
        this.mOffset2.x = offset.x;
        this.mOffset2.y = offset.y;

        var chart : Chart = this.getChartView();

        var width : number = this.mSize.width;
        var height : number = this.mSize.height;

        if (width == 0 && this.mDrawable != null) {
            width = this.mDrawable.getIntrinsicWidth();
        }
        if (height == 0 && this.mDrawable != null) {
            height = this.mDrawable.getIntrinsicHeight();
        }

        if (posX + this.mOffset2.x < 0) {
            this.mOffset2.x = - posX;
        } else if (chart != null && posX + width + this.mOffset2.x > chart.getWidth()) {
            this.mOffset2.x = chart.getWidth() - posX - width;
        }

        if (posY + this.mOffset2.y < 0) {
            this.mOffset2.y = - posY;
        } else if (chart != null && posY + height + this.mOffset2.y > chart.getHeight()) {
            this.mOffset2.y = chart.getHeight() - posY - height;
        }

        return this.mOffset2;
    }

//    @Override
    public refreshContent(e : EntryOhos, highlight : Highlight) : void {

    }

//    @Override
    public draw(canvas : Object/*Canvas*/, posX : number, posY : number) : void {

        if (this.mDrawable == null) return;

        var offset : MPPointF = this.getOffsetForDrawingAtPoint(posX, posY);

        var width : number = this.mSize.width;
        var height : number = this.mSize.height;

        if (width == 0) {
            width = this.mDrawable.getIntrinsicWidth();
        }
        if (height == 0) {
            height = this.mDrawable.getIntrinsicHeight();
        }

        this.mDrawable.copyBounds(this.mDrawableBoundsCache);
        this.mDrawable.setBounds(
                this.mDrawableBoundsCache.left,
                this.mDrawableBoundsCache.top,
                this.mDrawableBoundsCache.left + width,
                this.mDrawableBoundsCache.top + height);

        var saveId : number = canvas.save();
        // translate to the correct position and draw
        canvas.translate(posX + offset.x, posY + offset.y);
        this.mDrawable.draw(canvas);
        canvas.restoreToCount(saveId);

        this.mDrawable.setBounds(this.mDrawableBoundsCache);
    }
}
