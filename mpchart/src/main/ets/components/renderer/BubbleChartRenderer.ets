/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import MyRect from '../data/Rect';
import Highlight from '../highlight/Highlight';
import MPPointF from '../utils/MPPointF';
import { JArrayList } from '../utils/JArrayList';
import BubbleEntry from '../data/BubbleEntry';
import Transformer from '../utils/Transformer';
import BubbleData from '../data/BubbleData';
import BarLineScatterCandleBubbleRenderer from './BarLineScatterCandleBubbleRenderer'
import Paint,{Style, CirclePaint, ImagePaint, TextPaint} from '../data/Paint'
import Utils from '../utils/Utils'
import type IBubbleDataSet from '../interfaces/datasets/IBubbleDataSet'
import BubbleChartMode from '../data/BubbleChartMode';

export default class BubbleChartRenderer extends BarLineScatterCandleBubbleRenderer {

  bubbleChartMode:BubbleChartMode=new BubbleChartMode();
  constructor(bubbleChartMode:BubbleChartMode) {
    super(bubbleChartMode.mAnimator, bubbleChartMode.handler);
    this.bubbleChartMode = bubbleChartMode;
    this.mRenderPaint.setStyle(Style.FILL);

    this.mHighlightPaint.setStyle(Style.STROKE);
    this.mHighlightPaint.setStrokeWidth(Utils.convertDpToPixel(1.5));
  }

  public initBuffers() {

  }

  public compute(event: ClickEvent):Paint[]{
    let xResultScale=this.bubbleChartMode.centerX*this.bubbleChartMode.scaleX//-this.bubbleChartMode.centerX
    let yResultScale=this.bubbleChartMode.centerY*this.bubbleChartMode.scaleY//-this.bubbleChartMode.centerY

    //        let currentXSpace=this.bubbleChartMode.centerX*this.bubbleChartMode.scaleX-this.bubbleChartMode.centerX
    //        let currentYSpace=this.bubbleChartMode.centerY*this.bubbleChartMode.scaleY-this.bubbleChartMode.centerY
    //
    //         this.bubbleChartMode.xLeijiResultScale=currentXSpace-this.bubbleChartMode.xLeijiResultScale
    //        this.bubbleChartMode.yLeijiResultScale=currentYSpace-this.bubbleChartMode.yLeijiResultScale
    //        console.log("xxxxSourceScaleX:"+this.bubbleChartMode.centerX)
    //        console.log("xxxxSourceScaleY:"+this.bubbleChartMode.centerY)
    ////        console.log("xxxxScaleY:"+this.bubbleChartMode.centerX*this.bubbleChartMode.scaleX)
    ////        console.log("xxxxScaleY:"+this.bubbleChartMode.centerY*this.bubbleChartMode.scaleY)
    //        console.log("xxxxResultScaleX:"+xResultScale)
    //        console.log("xxxxyResultScaleY:"+yResultScale)
    //
    //        this.bubbleChartMode.moveX-=this.bubbleChartMode.xLeijiResultScale
    //        this.bubbleChartMode.moveY-=this.bubbleChartMode.yLeijiResultScale

    let paint:Paint[]=[];
    paint.push(this.drawClick(this.bubbleChartMode.centerX,this.bubbleChartMode.centerY,Color.Black))
    paint.push(this.drawClick(xResultScale,yResultScale,Color.Blue))
    paint.push(this.drawClick(xResultScale-this.bubbleChartMode.currentXSpace,yResultScale-this.bubbleChartMode.currentYSpace,0x55f54343))
    return paint
  }
  public computePathData(dispRect: MyRect, xScale: number, yScale: number,maxY: number){

  }
  public drawClick(x,y,color:number): Paint {
    let circlePaint:Paint = new CirclePaint();
    circlePaint.set(this.mRenderPaint);
    circlePaint.setStroke(color);
    circlePaint.setColor(color);
    circlePaint.setWidth(10);
    circlePaint.setHeight(10);

    circlePaint.setX(x)
    circlePaint.setY(y)
    circlePaint.setStrokeRadius(2);
    return circlePaint
  }
  public drawData(): Paint[] {

    let paints: Paint[] =new Array();
    let bubbleData: BubbleData = this.bubbleChartMode.data;

    for(let i = 0;i < bubbleData.getDataSets().length();i++){
      let dataSet = bubbleData.getDataSets().get(i)
      if (dataSet.isVisible()) {
        paints=paints.concat( this.drawDataSet(dataSet))
      }

    }
    return paints;
  }

  private sizeBuffer: number[] = new Array<number>(4);
  private pointBuffer: number[] = new Array<number>(2);

  public getShapeSize( entrySize:number, maxSize:number, reference:number, normalizeSize:boolean): number {
    const factor:number = normalizeSize ? ((maxSize == 0) ? 1 : Math.sqrt(entrySize / maxSize)) : entrySize;
    const shapeSize:number = reference * factor;
    return shapeSize;
  }

  protected drawDataSet( dataSet:IBubbleDataSet):Paint[] {
    let dataPaints: Paint[] =new Array();
    if (dataSet.getEntryCount() < 1)
    return;

    let trans:Transformer = new Transformer(this.mViewPortHandler);;

    let phaseY:number = this.mAnimator.getPhaseY();

    //this.mXBounds.set(this.mChart, dataSet);

    this.sizeBuffer[0] = 0;
    this.sizeBuffer[2] = 1;
    //
    trans.pointValuesToPixel(this.sizeBuffer);

    let normalizeSize:boolean = dataSet.isNormalizeSizeEnabled();
    let myRect:MyRect=this.bubbleChartMode.data.mDisplayRect;
    // calculate the full width of 1 step on the x-axis
    const maxBubbleWidth:number = this.bubbleChartMode.maxBubbleWidth;
    const maxBubbleHeight:number = Math.abs(myRect.bottom - myRect.top);

    const referenceSize:number = Math.min(maxBubbleHeight, maxBubbleWidth);
    for (let j = 0; j <dataSet.getEntryCount(); j++) {
      if(j>this.bubbleChartMode.maxVisiableIndex){
        break;
      }
      let bubbleEntry:BubbleEntry = dataSet.getEntryForIndex(j) as BubbleEntry;

      this.pointBuffer[0] = bubbleEntry.getX();
      this.pointBuffer[1] = bubbleEntry.getY()*phaseY ;
      trans.pointValuesToPixel(this.pointBuffer);

      let shapeHalf:number = this.getShapeSize(bubbleEntry.getSize(), dataSet.getMaxSize(), referenceSize, normalizeSize) / 2;
      // shapeHalf*=this.bubbleChartMode.scaleX

      // shapeHalf=px2vp(shapeHalf);
      //      if (!this.mViewPortHandler.isInBoundsTop(this.pointBuffer[1] + shapeHalf)
      //      || !this.mViewPortHandler.isInBoundsBottom(this.pointBuffer[1] - shapeHalf))
      //      continue;
      //
      //      if (!this.mViewPortHandler.isInBoundsLeft(this.pointBuffer[0] + shapeHalf))
      //      continue;
      //
      //      if (!this.mViewPortHandler.isInBoundsRight(this.pointBuffer[0] - shapeHalf))
      //      break;

      const color:number = dataSet.getColor(j);

      this.mRenderPaint.setColor(color);
      let circlePaint:Paint = new CirclePaint();
      circlePaint.set(this.mRenderPaint);
      circlePaint.setStroke(color);

      //位移屏幕物理点
      let xScaleResult=this.calcXDataResult(bubbleEntry.getX(),shapeHalf)*this.bubbleChartMode.scaleX+this.bubbleChartMode.moveX
      let yScaleResult=this.calcYDataResult((bubbleEntry.getY())*phaseY,shapeHalf)*this.bubbleChartMode.scaleY+this.bubbleChartMode.moveY

      let finalX=xScaleResult-this.bubbleChartMode.currentXSpace
      let finalY=yScaleResult-this.bubbleChartMode.currentYSpace
      circlePaint.setX(finalX)
      circlePaint.setY(finalY)

      let scaleShapeHalf=shapeHalf*this.bubbleChartMode.scaleX
      circlePaint.setWidth(scaleShapeHalf*2);
      circlePaint.setHeight(scaleShapeHalf * 2);
      circlePaint.setStrokeRadius(shapeHalf);
      dataPaints.push(circlePaint);


//      if(bubbleEntry.getX()>=this.bubbleChartMode.data.getXMax()){
//        let xScaleSourceResult=this.calcXDataResult(bubbleEntry.getX(),shapeHalf)
//        let yScaleSourceResult=this.calcYDataResult((bubbleEntry.getY())*phaseY,shapeHalf)
//
//        this.bubbleChartMode.xScaleRightSpace=(myRect.right-xScaleSourceResult)*this.bubbleChartMode.scaleX
//        this.bubbleChartMode.YScaleRightSpace=(myRect.bottom-yScaleSourceResult)*this.bubbleChartMode.scaleY
//
//        this.bubbleChartMode.maxPointX=finalX;
//        this.bubbleChartMode.maxPointY=finalY;
//      }
    }

    return dataPaints;
  }
  public calcXDataResult(x:number,shapeHalf:number):number{
    return x*this.bubbleChartMode.xScale+this.bubbleChartMode.data.mDisplayRect.left-shapeHalf
  }
  public calcYDataResult(y:number,shapeHalf:number):number{
    return this.bubbleChartMode.leftAxis.getAxisMaximum()*this.bubbleChartMode.yScale- y*this.bubbleChartMode.yScale+this.bubbleChartMode.data.mDisplayRect.top-shapeHalf
  }
  public calcVlaueWidthHalf(value:number):number{
    let resultValue= this.getFormattedValue(value);
    return Utils.calcTextWidth(this.mValuePaint,resultValue)
  }
  public getFormattedValue(value: number): string {
    return value.toFixed(1)+""
  }

  public drawValues(): Paint[] {
    let bubbleData: BubbleData = this.bubbleChartMode.data;

    if (bubbleData == null)
    return [];

    let paints: Paint[] = [];
    let phaseY:number = this.mAnimator.getPhaseY();
    // if values are drawn
    //if (this.isDrawingValuesAllowed(this.mChart)) {

    const dataSets:JArrayList<IBubbleDataSet> = bubbleData.getDataSets();
    for (let i = 0; i < dataSets.size(); i++) {

      let dataSet:IBubbleDataSet = dataSets.get(i);

      if (/*!this.shouldDrawValues(dataSet) ||*/ dataSet.getEntryCount() < 1){
        continue;
      }

      // apply the text-styling defined by the DataSet
      this.applyValueTextStyle(dataSet);
      //        const phaseX: number = Math.max(0, Math.min(1, this.mAnimator.getPhaseX()));
      //        const phaseY: number = this.mAnimator.getPhaseY();

      // this.mXBounds.set(this.mChart, dataSet);
      //        let trans:Transformer = new Transformer(this.mViewPortHandler);;
      //        const positions: number[] = trans
      //          .generateTransformedValuesBubble(dataSet, phaseY, this.mXBounds.min, this.mXBounds.max);
      //
      //        const alpha: number = phaseX == 1 ? phaseY : phaseX;
      //
      let iconsOffset:MPPointF = MPPointF.getInstance(null,null,dataSet.getIconsOffset());
      if(iconsOffset.x==null||iconsOffset.x==undefined){
        iconsOffset.x=0
      }
      if(iconsOffset.y==null||iconsOffset.y==undefined){
        iconsOffset.y=0
      }
      iconsOffset.x = Utils.convertDpToPixel(iconsOffset.x);
      iconsOffset.y = Utils.convertDpToPixel(iconsOffset.y);

      for (let j = 0; j < dataSet.getEntryCount(); j ++) {
        if(j>this.bubbleChartMode.maxVisiableIndex){
          break;
        }
        let valueTextColor: number =dataSet.getValueTextColor();
        //          valueTextColor = Color.argb(Math.round(255 * 1), Color.red(valueTextColor),
        //          Color.green(valueTextColor), Color.blue(valueTextColor));

        //          let x: number = positions[j];
        //          let y: number = positions[j + 1];

        //          if (!this.mViewPortHandler.isInBoundsRight(x))
        //          break;
        //
        //          if ((!this.mViewPortHandler.isInBoundsLeft(x) || !this.mViewPortHandler.isInBoundsY(y)))
        //          continue;
        let bubbleEntry: BubbleEntry = dataSet.getEntryForIndex(j) as BubbleEntry;


        if (dataSet.isDrawValuesEnabled()) {
          let widthHalf=this.calcVlaueWidthHalf(bubbleEntry.getSize())/2
          let heightHalf=this.mValuePaint.getTextSize()/2
          let x:number=this.calcXDataResult(bubbleEntry.getX(),widthHalf)*this.bubbleChartMode.scaleX;
          let y:number=this.calcYDataResult(bubbleEntry.getY()*phaseY ,heightHalf)*this.bubbleChartMode.scaleY
          paints=paints.concat(this.drawValue(dataSet.getValueFormatter(), bubbleEntry.getSize(), bubbleEntry, i,x+this.bubbleChartMode.moveX-this.bubbleChartMode.currentXSpace,
            y+this.bubbleChartMode.moveY-this.bubbleChartMode.currentYSpace, valueTextColor))
        }else{
          paints.push(new TextPaint())
        }

        if (bubbleEntry.getIcon() != null && dataSet.isDrawIconsEnabled()) {

          let icon: ImagePaint = bubbleEntry.getIcon();
          let drawIcon:ImagePaint=new ImagePaint(icon);
          let xIcon:number=this.calcXDataResult(bubbleEntry.getX(),Number(icon.getWidth())/2)*this.bubbleChartMode.scaleX;
          let yIcon:number=this.calcYDataResult(bubbleEntry.getY()*phaseY ,Number(icon.getHeight())/2)*this.bubbleChartMode.scaleY
          drawIcon.setX(xIcon+iconsOffset.x+this.bubbleChartMode.moveX-this.bubbleChartMode.currentXSpace)
          drawIcon.setY(yIcon+iconsOffset.y+this.bubbleChartMode.moveY-this.bubbleChartMode.currentYSpace)
          drawIcon.setWidth( Number(icon.getWidth()))
          drawIcon.setHeight(Number(icon.getHeight()))
          paints.push(drawIcon)
        }else{
          paints.push(new ImagePaint())
        }
      }

      //MPPointF.recycleInstance(iconsOffset);
    }
    //}
    return paints
  }

  public drawExtras(): Paint[] {
    return [];
  }

  private _hsvBuffer: Array<number> = new Array<number>(3);

  public drawHighlighted( indices:Highlight[]): Paint[] {
    let paints: Paint[] = [];
    if(indices[0].getDataSetIndex()>this.bubbleChartMode.maxVisiableIndex){
      return paints
    }
    let dataSet = this.bubbleChartMode.data.getDataSets().get(indices[0].getDataIndex());
    let bubbleEntry: BubbleEntry = dataSet.getEntryForIndex(indices[0].getDataSetIndex()) as BubbleEntry;

    let phaseY: number = this.mAnimator.getPhaseY();

    let normalizeSize:boolean = dataSet.isNormalizeSizeEnabled();
    let myRect:MyRect=this.bubbleChartMode.data.mDisplayRect;
    // calculate the full width of 1 step on the x-axis
    const maxBubbleWidth:number = this.bubbleChartMode.maxBubbleWidth;
    const maxBubbleHeight:number = Math.abs(myRect.bottom - myRect.top);

    const referenceSize:number = Math.min(maxBubbleHeight, maxBubbleWidth);

    let trans:Transformer = new Transformer(this.mViewPortHandler);;
    this.pointBuffer[0] = bubbleEntry.getX();
    this.pointBuffer[1] = bubbleEntry.getY() ;
    trans.pointValuesToPixel(this.pointBuffer);

    let shapeHalf:number = this.getShapeSize(bubbleEntry.getSize(), dataSet.getMaxSize(), referenceSize, normalizeSize) / 2;
    // shapeHalf=px2vp(shapeHalf);
    //      if (!this.mViewPortHandler.isInBoundsTop(this.pointBuffer[1] + shapeHalf)
    //      || !this.mViewPortHandler.isInBoundsBottom(this.pointBuffer[1] - shapeHalf))
    //      continue;
    //
    //      if (!this.mViewPortHandler.isInBoundsLeft(this.pointBuffer[0] + shapeHalf))
    //      continue;
    //
    //      if (!this.mViewPortHandler.isInBoundsRight(this.pointBuffer[0] - shapeHalf))
    //      break;

    const color:number = 0xb3a14a;

    this.mRenderPaint.setColor(color);
    let circlePaint:Paint = new CirclePaint();
    circlePaint.set(this.mRenderPaint);
    circlePaint.setStroke(color);

    circlePaint.setX(this.calcXDataResult(bubbleEntry.getX(),shapeHalf)*this.bubbleChartMode.scaleX-2+this.bubbleChartMode.moveX-this.bubbleChartMode.currentXSpace)
    circlePaint.setY(this.calcYDataResult(bubbleEntry.getY()*phaseY,shapeHalf)*this.bubbleChartMode.scaleX-2+this.bubbleChartMode.moveY-this.bubbleChartMode.currentYSpace)

    shapeHalf*=this.bubbleChartMode.scaleX
    circlePaint.setWidth(shapeHalf*2+4);
    circlePaint.setHeight(shapeHalf * 2+4);

    circlePaint.setStrokeRadius(shapeHalf);
    paints.push(circlePaint);
    return paints;
  }
}
