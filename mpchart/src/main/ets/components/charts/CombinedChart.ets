/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import EntryOhos from '../data/EntryOhos';
import MyCandleEntry from '../data/MyCandleEntry';
import BubbleEntry from '../data/BubbleEntry';
import BarEntry from '../data/BarEntry';
import LineData from '../data/LineData';
import ScatterData from '../data/ScatterData';
import BubbleData from '../data/BubbleData';
import BarData from '../data/BarData';
import {LineDataSet,Mode} from '../data/LineDataSet';
import CombinedData from '../data/CombinedData';
import MyCandleData from '../data/MyCandleData';
import {ScatterDataSet} from '../data/ScatterDataSet';
import MyCandleDataSet from '../data/MyCandleDataSet';
import BubbleDataSet from '../data/BubbleDataSet';
import BarDataSet from '../data/BarDataSet';
import type IBarDataSet from '../interfaces/datasets/IBarDataSet'
import type ICandleDataSet from '../interfaces/datasets/ICandleDataSet'

import { JArrayList } from '../utils/JArrayList';
import MyRect from '../data/Rect';
import { TextPaint , ImagePaint} from '../data/Paint';
import {XAxis,XAxisPosition} from '../components/XAxis';
import XAxisView from '../components/renderer/XAxisView';
import YAxisView, { YAxisModel }  from '../components/renderer/YAxisView'
import YAxis,{YAxisLabelPosition,AxisDependency} from '../components/YAxis'
import Utils from '../utils/Utils'
import LegendEntry from '../components/LegendEntry';
import MultipleLegend from '../components/renderer/MultipleColorLegend';
import AxisBase from '../components/AxisBase';
import type IAxisValueFormatter from '../formatter/IAxisValueFormatter';

import PathView, { PathViewModel } from '../components/PathView'
import ScatterView from '../components/ScatterView'
import BubbleView from '../components/renderer/BubbleView'
import { BarChartModel } from './BarChart';
import BarChart from './BarChart';
import BubbleChartMode from '../data/BubbleChartMode';
import ScaterChartMode from '../data/ScaterChartMode';
import CandleStickChart from '../charts/CandleStickChart';
import MPPointF from '../utils/MPPointF';
import ScaleMode from '../data/ScaleMode';
import LineChart,{ LineChartModel } from './LineChart';

@Entry
@Component
export default struct CombinedChart  {
  //@State
  topAxis: XAxis = new XAxis(); //顶部X轴
  //@State
  bottomAxis: XAxis = new XAxis(); //底部X轴
  //@State
  mWidth: number = 350; //表的宽度
  //@State
  mHeight: number = 600; //表的高度
  //@State
  minOffset: number = 15; //X轴线偏移量
  //@State
  leftAxis: YAxis = new YAxis();
  //@State
  rightAxis: YAxis = new YAxis();
  //@State
  dataBar:BarData= new BarData();
  @State legendBar:LegendEntry=new LegendEntry();
  @State legendBarStack1:LegendEntry=new LegendEntry();
  @State legendBarStack2:LegendEntry=new LegendEntry();

  dataBubble:BubbleData= new BubbleData();
  @State legendBubble:LegendEntry=new LegendEntry();

  dataLine:LineData= new LineData();
  @State legendLine:LegendEntry=new LegendEntry();

  dataScatter:ScatterData = new ScatterData();
  @State legendScatter:LegendEntry=new LegendEntry();

  dataCandle:MyCandleData = new MyCandleData();
  @State legendCandle:LegendEntry=new LegendEntry();

  data:CombinedData = new CombinedData();
  count:number = 12;
  // @State
  legendColorWidth: number = 10;
  // @State
  legendColorHeight: number = 10;
  //@State
  clipPath:string=''
  //@State
  private rootViewBgColor: number | string | Color = Color.White; //chart区域的背景色
  private chartBgColor: number | string | Color = "#00FFFFFF"; //根布局的背景色
  isInverted: boolean = false;
  @State model:BarChartModel = new BarChartModel();
  @State
  bubbleChartMode:BubbleChartMode=new BubbleChartMode();
  @State
  scaterChartMode: ScaterChartMode= new ScaterChartMode();
  pathViewModel: PathViewModel = new PathViewModel();
  @State
  lineChartModel: LineChartModel = new LineChartModel();

  @State chartModel: CandleStickChart.Model = new CandleStickChart.Model()
  @State
  scaleMode:ScaleMode=new ScaleMode()
  @State
  leftAxisModel: YAxisModel = new YAxisModel();
  @State
  rightAxisModel: YAxisModel = new YAxisModel();
  build() {
    Column() {
      Stack({ alignContent: Alignment.TopStart }) {
        XAxisView({
          scaleMode:this.scaleMode
        });
        YAxisView({ model:this.leftAxisModel})
        YAxisView({ model:this.rightAxisModel})
        ScatterView({ scaterChartMode: $scaterChartMode })
        CandleStickChart({ model: this.chartModel ,visibilityAxis: Visibility.Hidden})
        BarChart({ model: this.model, visibilityAxis: Visibility.Hidden })
        //PathView({ model: this.pathViewModel })
        LineChart({lineChartModel: $lineChartModel})
        BubbleView({ bubbleChartMode: $bubbleChartMode })
      }.height(this.mHeight)

      Row() {
        MultipleLegend({ legend: this.legendLine });
        Blank().width(4)
        MultipleLegend({ legend: this.legendBar });
        Blank().width(4)
        MultipleLegend({ legend: this.legendBarStack1 });
        Blank().width(4)
        MultipleLegend({ legend: this.legendBarStack2 });
      }
      Row(){
        MultipleLegend({legend:this.legendScatter});
        Blank().width(4)
        MultipleLegend({legend:this.legendCandle});
      }.margin({top:5})
      Blank().height(5)
      MultipleLegend({legend:this.legendBubble});
    }.width(this.mWidth)
  }
  public aboutToAppear() {
    this.dataLine=this.generateLineData();
    this.data.setData(this.dataLine);

    this.dataScatter=this.generateScatterData();
    this.data.setData( this.dataScatter);

    this.dataCandle = this.generateCandleData();
    this.chartModel.setCandleData(this.dataCandle)


    this.dataBubble=this.generateBubbleData();
    this.data.setData( this.dataBubble);

    this.dataBar= this.generateBarData();
    this.data.setData( this.dataBar);

    let textPaint:TextPaint = new TextPaint();
    textPaint.setTextSize(this.leftAxis.getTextSize());

    var leftTextWidth = Utils.calcTextWidth(textPaint,this.getFormattedValue(this.data.getYMax()));
    var rightTextWidth = Utils.calcTextWidth(textPaint,this.getFormattedValue(this.data.getYMax()));
    let left = this.minOffset + leftTextWidth;
    let top = this.minOffset;
    let right = this.mWidth - this.minOffset - rightTextWidth;
    let bottom = this.mHeight - this.minOffset;
    let myRect:MyRect = new MyRect(left, top, right, bottom);
    this.dataLine.mDisplayRect = myRect;
    this.dataScatter.mDisplayRect = myRect;
    this.dataBubble.mDisplayRect = myRect;

    this.clipPath='M'+Utils.convertDpToPixel(myRect.left)+' '+Utils.convertDpToPixel(myRect.top-this.minOffset)
    +'L'+Utils.convertDpToPixel(myRect.right)+' '+Utils.convertDpToPixel(myRect.top-this.minOffset)
    +'L'+Utils.convertDpToPixel(myRect.right)+' '+Utils.convertDpToPixel(myRect.bottom-this.minOffset)
    +'L'+Utils.convertDpToPixel(myRect.left)+' '+Utils.convertDpToPixel(myRect.bottom-this.minOffset)
    +' Z'
    let months:string[] =[
      "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Okt", "Nov", "Dec"
    ];
    this.topAxis.setLabelCount(7, false);
    this.topAxis.setPosition(XAxisPosition.TOP);
    this.topAxis.setAxisMinimum(0);
    this.topAxis.setAxisMaximum(this.count);
    this.topAxis.enableGridDashedLine(10,10,0)
    this.topAxis.setDrawAxisLine(true);
    this.topAxis.setDrawLabels(true)
    this.topAxis.setValueFormatter(new class implements IAxisValueFormatter {
      public  getFormattedValue( value:number,  axis:AxisBase):string {
        return months[value % months.length];
      }
    });

    this.bottomAxis.setLabelCount(7, false);
    this.bottomAxis.setPosition(XAxisPosition.BOTTOM);
    this.bottomAxis.setAxisMinimum(0);
    this.bottomAxis.setAxisMaximum(this.count);
    this.bottomAxis.setDrawAxisLine(true);
    this.bottomAxis.setDrawLabels(true)
    this.bottomAxis.setValueFormatter(new class implements IAxisValueFormatter {
      public  getFormattedValue( value:number,  axis:AxisBase):string {
        return months[value % months.length];
      }
    });

    this.leftAxis = new YAxis(AxisDependency.LEFT);
    this.leftAxis.setLabelCount(7, false);
    this.leftAxis.setPosition(YAxisLabelPosition.OUTSIDE_CHART);
    this.leftAxis.setSpaceTop(15);
    this.leftAxis.setAxisMinimum(0);
    this.leftAxis.setAxisMaximum(this.data.getYMax()+10);
    this.leftAxis.setDrawGridLines(false);
    this.leftAxis.enableGridDashedLine(10,10,0)

    this.rightAxis = new YAxis(AxisDependency.RIGHT);
    this.rightAxis.setDrawGridLines(false);
    this.rightAxis.setLabelCount(7, false);
    this.rightAxis.setSpaceTop(15);
    this.rightAxis.setAxisMinimum(0); // this replaces setStartAtZero(true)
    this.rightAxis.setAxisMaximum(this.data.getYMax()+10);
    this.rightAxis.setDrawAxisLine(true);
    this.rightAxis.setDrawLabels(true)

    this.initPathViewModel();

    this.model.mWidth = this.mWidth;
    this.model.mHeight = this.mHeight;
    this.model.init();
    this.model.setDrawBarShadow(false);
    this.model.setDrawValueAboveBar(true);
    this.model.getDescription().setEnabled(false);
    this.model.setMaxVisibleValueCount(24);
    this.model.setLeftYAxis(this.leftAxis);
    this.model.setRightYAxis(this.rightAxis);
    this.model.setXAxis(this.bottomAxis)
    this.model.mRenderer.initBuffers();
    this.model.prepareMatrixValuePx();

    this.bubbleChartMode.maxBubbleWidth=25
    this.bubbleChartMode.setYExtraOffset(this.model.mHeight)
    this.bubbleChartMode.topAxis=this.topAxis
    this.bubbleChartMode.bottomAxis=this.bottomAxis
    this.bubbleChartMode.mWidth=this.mWidth
    this.bubbleChartMode.mHeight=this.mHeight
    this.bubbleChartMode.minOffset=this.minOffset
    this.bubbleChartMode.leftAxis=this.leftAxis
    this.bubbleChartMode.rightAxis=this.rightAxis
    this.bubbleChartMode.data=this.dataBubble
    this.bubbleChartMode.isNeedScale=false
    this.bubbleChartMode.init()

    //this.scaterChartMode.setYExtraOffset(this.model.height)
    this.scaterChartMode.topAxis=this.topAxis
    this.scaterChartMode.bottomAxis=this.bottomAxis
    this.scaterChartMode.mWidth=this.mWidth
    this.scaterChartMode.mHeight=this.mHeight
    this.scaterChartMode.minOffset=this.minOffset
    this.scaterChartMode.leftAxis=this.leftAxis
    this.scaterChartMode.rightAxis=this.rightAxis
    this.scaterChartMode.data=this.dataScatter
    this.scaterChartMode.init()

    this.chartModel.mWidth=this.mWidth
    this.chartModel.mHeight=this.mHeight

    this.setLineLengend();
    this.setSctterLengend();
    this.setLegendCandle();
    this.setlegendBubble();
    this.setlegendBar();
    this.setXAxisMode();
    this.setYAisMode();
  }
  public setXAxisMode(){

    this.scaleMode.xAixsMode.topAxis=this.topAxis
    this.scaleMode.xAixsMode.bottomAxis=this.bottomAxis,
    this.scaleMode.xAixsMode.mWidth= this.mWidth,
    this.scaleMode.xAixsMode.mHeight=this.mHeight,
    this.scaleMode.xAixsMode.minOffset=this.minOffset,
    this.scaleMode.xAixsMode.yLeftLongestLabel=this.getFormattedValue(this.leftAxis.getAxisMaximum()),
    this.scaleMode.xAixsMode.yRightLongestLabel=this.getFormattedValue(this.rightAxis.getAxisMaximum())

  }
  public setYAisMode(){
    this.leftAxisModel.setWidth(this.mWidth)
    this.leftAxisModel.setHeight(this.mHeight)
    this.leftAxisModel.setMinOffset(this.minOffset)
    this.leftAxisModel.setYAxis(this.leftAxis)

    this.rightAxisModel.setWidth(this.mWidth)
    this.rightAxisModel.setHeight(this.mHeight)
    this.rightAxisModel.setMinOffset(this.minOffset)
    this.rightAxisModel.setYAxis(this.rightAxis)
  }
  public generateLineData():LineData{
    let d:LineData = new LineData();
    let entries:JArrayList<EntryOhos> =  new JArrayList<EntryOhos>()
    for (let index = 0; index < this.count; index++){
      entries.add(new EntryOhos(index+0.5, this.getRandom(15, 5)));
      //entries.add(new EntryOhos(index, 20));
    }
    let dataSet:LineDataSet = new LineDataSet(entries, "Line DataSet");
    dataSet.setColorByColor(0xf0ee46);
    dataSet.setLineWidth(2.5);
    dataSet.setCircleColor(0xf0ee46);
    dataSet.setCircleRadius(5);
    dataSet.setFillColor(0xf0ee46);
    dataSet.setMode(Mode.CUBIC_BEZIER);
    dataSet.setDrawValues(true);
    dataSet.setValueTextSize(10);
    dataSet.setValueTextColor(0xf0ee46);
    dataSet.setAxisDependency(AxisDependency.LEFT);
    d.addDataSet(dataSet);



    return d;
  }
  private  generateBarData() :BarData{

    let dataSets: JArrayList<IBarDataSet> = new JArrayList<IBarDataSet>();

    for (let index = 0; index < this.count; index++) {
      let entries1:JArrayList<BarEntry> =  new JArrayList<BarEntry>()
      let entries2:JArrayList<BarEntry> =  new JArrayList<BarEntry>()
      //entries1.add(new BarEntry(0, this.getRandom(25, 25)));
      entries1.add(new BarEntry(0, [this.getRandom(25, 25),this.getRandom(0, 0)]));

      // stacked
      entries2.add(new BarEntry(0, [this.getRandom(13, 12), this.getRandom(13, 12)]));
      //entries1.add(new BarEntry(0, [this.getRandom(13, 12), this.getRandom(13, 12)]));
      let set1:BarDataSet = new BarDataSet(entries1, "Bar 1");
      set1.setColorByColor(0x3cdc4e);
      set1.setValueTextColor(0x3cdc4e);
      set1.setValueTextSize(10);
      set1.setAxisDependency(AxisDependency.LEFT);

      let set2:BarDataSet = new BarDataSet(entries2, "");
      set2.setStackLabels(["Stack 1", "Stack 2"]);
      set2.setColorsByArr([0x3da5ff, 0x17c5ff]);
      set2.setValueTextColor(0x3da5ff);
      set2.setValueTextSize(10);
      set2.setAxisDependency(AxisDependency.LEFT);

      dataSets.add(set1);
      dataSets.add(set2);
    }

    let groupSpace:number = 0.06;
    let barSpace:number = 0.02; // x2 dataset
    let barWidth:number = 0; // x2 dataset
    // (0.45 + 0.02) * 2 + 0.06 = 1.00 -> interval per "group"
    //let dataSets: JArrayList<IBarDataSet> = new JArrayList<IBarDataSet>();
    //dataSets.add(set1);
    // dataSets.add(set2);

    let d:BarData = new BarData(dataSets);
    d.setBarWidth(barWidth);
    // make this BarData object grouped
    //d.groupBars(0, groupSpace, barSpace); // start at x = 0
    this.model.setData(d);

    return this.model.getBarData();
  }

  private initPathViewModel(){
    this.lineChartModel.setTopAxis(this.topAxis);
    this.lineChartModel.setBottomAxis(this.bottomAxis);
    this.lineChartModel.setWidth(this.mWidth);
    this.lineChartModel.setHeight(this.mHeight);
    this.lineChartModel.setMinOffset(this.minOffset);
    this.lineChartModel.setLeftAxis(this.leftAxis);
    this.lineChartModel.setRightAxis(this.rightAxis);
    this.lineChartModel.setLineData(this.dataLine);
    this.lineChartModel.rootViewBgColor=0x01000000
    this.lineChartModel.isShowLegend=false
    this.lineChartModel.isShowXAxis=false
    this.lineChartModel.init();
  }

  private  generateScatterData():ScatterData {

    let d:ScatterData = new ScatterData();
    let entries:JArrayList<EntryOhos> =  new JArrayList<EntryOhos>()
    for (let index = 0; index < this.count; index += 0.5){
      entries.add(new EntryOhos(index + 0.25, this.getRandom(10, 55)));
    }
    let dataSet:ScatterDataSet = new ScatterDataSet(entries, "Scatter DataSet");
    dataSet.setColorsByArr([0x2ecc71,0xf1c40f,0xe74c3c,0x3498db]);
    dataSet.setScatterShapeSize(15);
    dataSet.setDrawValues(false);
    dataSet.setValueTextSize(10);
    dataSet.setIconsOffset(new MPPointF(0, px2vp(0)));
    d.addDataSet(dataSet);
    return d;
  }
  private  generateCandleData():MyCandleData {

    let d:MyCandleData = new MyCandleData();
    let entries:JArrayList<MyCandleEntry> =  new JArrayList<MyCandleEntry>()
    let start:number=55;
    for (let index = 0; index < this.count; index+=2){
      entries.add(new MyCandleEntry(start,255, 180, 230, 200));
      start+=(index==this.count-4)?45:50;

    }
    //    entries.add(new MyCandleEntry(55, 255, 180, 230, 200));
    //    entries.add(new MyCandleEntry(105, 255, 180, 230, 200));
    //    entries.add(new MyCandleEntry(155, 255, 180, 230, 200));
    //    entries.add(new MyCandleEntry(205, 255, 180, 230, 200));
    //    entries.add(new MyCandleEntry(255, 255, 180, 230, 200));
    //    entries.add(new MyCandleEntry(300, 255, 180, 230, 200));
    //    let dataSet:MyCandleDataSet = new MyCandleDataSet(entries, "Candle DataSet");
    //    dataSet.setDecreasingColor(0x8e96af);
    //    dataSet.setShadowColor(0xFF444444);
    //    dataSet.setBarSpace(0.3);
    //    dataSet.setValueTextSize(10);
    //    dataSet.setDrawValues(false);
    //    d.addDataSet(dataSet);

    let dataSets = new JArrayList<MyCandleDataSet>();

    let set1 = new MyCandleDataSet(entries, "Candle DataSet");
    set1.setShadowColor(0xFF444444);
    set1.setDecreasingColor(0x8e96af);
    dataSets.add(set1);

    return new MyCandleData(dataSets);

    //return d;
  }
  private  generateBubbleData():BubbleData {
    let bd:BubbleData = new BubbleData();
    let entries:JArrayList<BubbleEntry> =  new JArrayList<BubbleEntry>()

    for (let index = 0.5; index < this.count; index++) {
      let y:number = this.getRandom(10, 105);
      let size:number = this.getRandom(100, 105);
      if(index==this.count-0.5){
        entries.add(new BubbleEntry(index-0.5 , y, size));
      }else{
        entries.add(new BubbleEntry(index , y, size));
      }

    }

    let dataSet:BubbleDataSet = new BubbleDataSet(entries, "Bubble DataSet");
    dataSet.setColorsByArr([0xc0ff8c,0xfff78c,0xffd08c,0x8ceaff,0xff8c9d]);
    dataSet.setValueTextSize(9);
    dataSet.setValueTextColor(Color.White);
    dataSet.setHighlightCircleWidth(1.5);
    dataSet.setDrawValues(true);
    bd.addDataSet(dataSet);

    return bd;
  }
  private getFormattedValue(value: number): string {
    return value.toFixed(0)
  }
  private  getRandom( range:number, start:number):number {
    return  (Math.random() * range) + start;
  }
  public setLineLengend(){
    this.legendLine.colorWidth=this.legendColorWidth
    this.legendLine.colorHeight=this.legendColorHeight
    this.legendLine.colorItemSpace=3
    this.legendLine.colorLabelSpace=4
    this.legendLine.labelColor=Color.Black
    this.legendLine.labelTextSize=10

    for(let i=0; i<this.dataLine.getDataSets().size(); i++) {
      let dataSet = this.dataLine.getDataSetByIndex(i)
      this.legendLine.colorArr=dataSet.getColors().dataSource
      this.legendLine.label=dataSet.getLabel()
    }

  }
  public setSctterLengend(){

    this.legendScatter.colorWidth=this.legendColorWidth
    this.legendScatter.colorHeight=this.legendColorHeight
    this.legendScatter.colorItemSpace=3
    this.legendScatter.colorLabelSpace=4
    this.legendScatter.labelColor=Color.Black
    this.legendScatter.labelTextSize=10

    for(let i=0; i<this.dataScatter.getDataSets().size(); i++) {
      let dataSet = this.dataScatter.getDataSetByIndex(i)
      this.legendScatter.colorArr=dataSet.getColors().dataSource
      this.legendScatter.label=dataSet.getLabel()
    }
  }
  public setLegendCandle(){

    this.legendCandle.colorWidth=this.legendColorWidth
    this.legendCandle.colorHeight=this.legendColorHeight
    this.legendCandle.colorItemSpace=3
    this.legendCandle.colorLabelSpace=15
    this.legendCandle.labelColor=Color.Black
    this.legendCandle.labelTextSize=10

    for(let i=0; i<this.dataCandle.getDataSetCount(); i++) {
      let dataSet:ICandleDataSet = this.dataCandle.getDataSetByIndex(i)
      this.legendCandle.colorArr=[dataSet.getDecreasingColor()];
      this.legendCandle.label=dataSet.getLabel()
    }
  }
  public setlegendBubble(){

    this.legendBubble.colorWidth=this.legendColorWidth
    this.legendBubble.colorHeight=this.legendColorHeight
    this.legendBubble.colorItemSpace=3
    this.legendBubble.colorLabelSpace=4
    this.legendBubble.labelColor=Color.Black
    this.legendBubble.labelTextSize=10

    for(let i=0; i<this.dataBubble.getDataSets().size(); i++) {
      let dataSet = this.dataBubble.getDataSetByIndex(i)
      this.legendBubble.colorArr=dataSet.getColors().dataSource
      this.legendBubble.label=dataSet.getLabel()
    }
  }
  public setlegendBar(){
    this.legendBar.colorWidth=this.legendColorWidth
    this.legendBar.colorHeight=this.legendColorHeight
    this.legendBar.colorItemSpace=3
    this.legendBar.colorLabelSpace=4
    this.legendBar.labelColor=Color.Black
    this.legendBar.labelTextSize=10
    this.legendBar.colorArr=[0x3cdc4e]
    this.legendBar.label="Bar 1"

    this.legendBarStack1.colorWidth=this.legendColorWidth
    this.legendBarStack1.colorHeight=this.legendColorHeight
    this.legendBarStack1.colorItemSpace=3
    this.legendBarStack1.colorLabelSpace=4
    this.legendBarStack1.labelColor=Color.Black
    this.legendBarStack1.labelTextSize=10
    this.legendBarStack1.colorArr=[0x3da5ff]
    this.legendBarStack1.label="Stack 1"

    this.legendBarStack2.colorWidth=this.legendColorWidth
    this.legendBarStack2.colorHeight=this.legendColorHeight
    this.legendBarStack2.colorItemSpace=3
    this.legendBarStack2.colorLabelSpace=4
    this.legendBarStack2.labelColor=Color.Black
    this.legendBarStack2.labelTextSize=10
    this.legendBarStack2.colorArr=[0x17c5ff]
    this.legendBarStack2.label="Stack 2"
  }


}

